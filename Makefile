## ZIP
DIR_NAME = LINFO1115_PROJECT_2022_GJadin_BLambert
ZIP_NAME = ./$(DIR_NAME).zip
FILES = src Makefile README.md report.pdf

## ARCHIVING
archive:
	@mkdir ${DIR_NAME}
	@$(foreach file,${FILES},cp -r ${file} ${DIR_NAME}/${file};)
	#@git log --stat > ${DIR_NAME}/gitlog.stat
	zip -r ${ZIP_NAME} ${DIR_NAME}
	@rm -r -d ${DIR_NAME}

remove:
	rm ${ZIP_NAME}

.PHONY: archive