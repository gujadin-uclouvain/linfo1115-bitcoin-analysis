from graph import Graph
import csv_extraction


def basic_properties(dataframe):
    """
    Input: Pandas dataframe as described above representing a graph
    Output: (number_of_different_components, number_of_bridges, number_of_local_bridges)
    """
    graph = Graph()
    graph.generate(dataframe, is_bidirectional=True)
    return graph.get_components(compute_local_bridges=True), graph.get_bridges(), graph.get_local_bridges()


def total_triadic_closures(dataframe):
    """
    Input: Pandas dataframe as described above representing a graph
    Output: Returns the total amount of triadic closures that arrise between the median timestamp of the dataframe until the last timestamp.
    Reminder: The triadic closures do not take into account the sign of the edges.
    """
    graph = Graph()
    graph.generate(dataframe, is_bidirectional=True, priority="older_timestamp")
    return graph.get_triadic_closures()[0]


def end_balanced_degree(dataframe):
    """
    Input: Pandas dataframe as described above representing a graph
    Output: Returns the final balance degree of the graph (as defined in the project statement).
    Reminder: Take into account that the graph is weighted now.
    """
    graph = Graph()
    graph.generate(dataframe, is_bidirectional=True, priority="newer_timestamp")
    return graph.get_balance_degree()[0]


def distances(dataframe):
    """
    Input: Pandas dataframe as described above representing a graph
    Output: Returns a list where the element at index i represents the total number of small paths of distances i+1 in the graph.
    Reminder: Take into account that the graph is directed now.
    """
    return [0, 0, 0, 0, 0]


def pagerank(dataframe):
    """
    Input: Pandas dataframe as described above representing a graph
    Output: (index, pagerank_value)
    where the index represents the id of the node with the highest pagerank value and pagerank_value its associated pagerank value after convergence.
    (we consider that we reached convergence when the sum of the updates on all nodes after one iteration of PageRank is smaller than 10^(-10))
    Reminder: Take into account that the graph is directed now.
    """
    return 0, 0.0


if __name__ == "__main__":
    dataset = csv_extraction.DatasetTreatment()
    dataset.open()
    print(basic_properties(dataset.get()))
    print(total_triadic_closures(dataset.get()))
    print(end_balanced_degree(dataset.get()))
    dataset.close()
