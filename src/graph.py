import pandas as pd
import numpy as np
from types import MappingProxyType

class Node:
    """
    A class Node to represent a person.
    It can be a buyer or a seller.

    :param int                     self.id:    The name/id of the current node
    :param {int: MappingProxyType} self.links: A dictionary containing for each key (a node id), an immutable dictionary
                                               (the edge between 'self' and the 'node' id)
    """

    def __init__(self, name):
        """
        The constructor for the Node class

        :param int name: The name/id of the node (buyer or seller)
        """
        self.id = name
        self.links = {}

    def __hash__(self): return hash(self.id)
    def __str__(self): return "Node #" + str(self.id)
    def __len__(self): return len(self.links)
    def __eq__(self, node): return self.id == node.id
    def __ne__(self, node): return self.id != node.id
    def __lt__(self, node): return self.id < node.id
    def __le__(self, node): return self.id <= node.id
    def __gt__(self, node): return self.id > node.id
    def __ge__(self, node): return self.id >= node.id

    def __check_is_different(self, node):
        """
        Check if the two nodes are different

        :param Node node: An instance of the class Node
        :raise ValueError: If the two nodes have the same id
        """
        if self == node:
            raise ValueError("These are the same nodes")

    def __has_older_timestamp(self, node, timestamp):
        """
        Check if the timestamp is strictly older than the timestamp of the current edge between 'self' and 'node'

        :param Node node: An instance of the class Node
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :return: True if older, False otherwise
        :rtype: bool
        """
        return timestamp < self.links[node.id]["timestamp"]

    def __has_newer_timestamp(self, node, timestamp):
        """
        Check if the timestamp is strictly newer than the timestamp of the current edge between 'self' and 'node'

        :param Node node: An instance of the class Node
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :return: True if newer, False otherwise
        :rtype: bool
        """
        return timestamp > self.links[node.id]["timestamp"]

    def strength_bridge(self, node):
        """
        Compute the Neighborhood overlap to determine if an edge between two nodes is a local bridge or not.
        The equation is the following:

                     (Links Intersection of A and B)
               --------------------------------------------  =  strength of the bridge A-B
                (Links Union of A and B (without A and B))

        If the result is exactly 0  =>           Local Bridge
        If the result is almost  0  =>  May be a Local Bridge

        :param Node node: An instance of the class Node
        :return: The result of the equation above
        :rtype: float
        """
        len_union = len(self.union(node))
        return 0 if len_union == 0 else len(self.intersection(node)) / len_union

    def intersection(self, node):
        """
        Find the intersection between 'self' links and 'node' links

        :param Node node: An instance of the class Node
        :return: a set of nodes id that are the intersection between A and B
        :rtype: set(int)
        """
        self.__check_is_different(node)
        return self.links.keys() & node.links.keys()

    def union(self, node):
        """
        Find the union of 'self' links and 'node' links (without 'self' and 'node')

        :param Node node: An instance of the class Node
        :return: a set of nodes id that are the union of A and B
        :rtype: set(int)
        """
        self.__check_is_different(node)
        union = self.links.keys() | node.links.keys()
        union.remove(self.id)
        union.remove(node.id)
        return union

    def add(self, node, rating=0, timestamp=0.0, priority=None):
        """
        Add an edge (a transaction) from 'self' to 'node'

        :param Node node: An instance of the class Node
        :param int rating: The level of reliability between A and B
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :param Node|str priority: If there are multiple transactions between the same two nodes,
                                  priority select the action plan (if type(priority) == str)
                                  None select the first edge analysed
        """
        self.__check_is_different(node)
        self.links.setdefault(node.id, MappingProxyType({"node": node, "rating": rating, "timestamp": timestamp}))
        if priority == "older_timestamp" and self.__has_older_timestamp(node, timestamp): # Update with older Timestamp
            self.links[node.id] = MappingProxyType({"node": node, "rating": rating, "timestamp": timestamp})
        elif priority == "newer_timestamp" and self.__has_newer_timestamp(node, timestamp):
            self.links[node.id] = MappingProxyType({"node": node, "rating": rating, "timestamp": timestamp})

    def each_add(self, node, rating=0, timestamp=0.0, priority=None):
        """
        Add an edge (a transaction) from 'self' to 'node' and from 'node' to 'self'

        :param Node node: An instance of the class Node
        :param int rating: The level of reliability between A and B
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :param Node|str priority: If there are multiple transactions between the same two nodes,
                                  priority select the action plan (if type(priority) == str)
                                  None select the first edge analysed
        """
        self.add(node, rating, timestamp, priority)
        node.add(self, rating, timestamp, priority)

class Graph:
    """
    A Graph class which represent a Benchmark to calculate multiples things in a generate dataframes
    This class use the class Node to represent nodes and edges

    :param {int: Node}     self.__node:                The name/id of the current node
    :param set[tuple[4]]   self.__edge:                The set of all the edge there are in the graph
    :param None|list[Node] self.__roots_of_components: A list of nodes which each node represent a graph component
    :param None|list[str]  self.__bridges:             A list of strings representing edges that are bridges
    :param None|list[str]  self.__local_bridges:       A list of strings representing edges that are local bridges
    """

    def __init__(self):
        """
        The constructor of the Graph class
        """
        self.__nodes = {}
        self.__edges = set()
        self.__roots_of_components = None
        self.__bridges = None
        self.__local_bridges = None

    ## Graph Management
    # Reset Methods
    def __reset_variables(self):
        """
        If 'self.__roots_of_components' or 'self.__bridges' or 'self.__local_bridges' are already computed (is not None)
        Reset these variables
        """
        if self.__roots_of_components is not None or self.__bridges is not None or self.__local_bridges is not None:
            self.__roots_of_components, self.__bridges, self.__local_bridges = None, None, None

    def __reset_graph(self):
        """
        Reset the variables and the variables 'self.__nodes' and 'self.__edges'
        """
        self.__nodes = {}
        self.__edges = set()
        self.__reset_variables()

    # Setters
    def set_node(self, id_node):
        """
        Add a node in the graph (without any edges)

        :param int id_node: The name/id of a node
        """
        self.__reset_variables()
        self.__nodes[id_node] = Node(id_node)

    def set_bidirectional_edge(self, id1, id2, rating=0, timestamp=0.0, priority=None):
        """
        Add a bidirectional edge between node1 and node2

        :param int id1: The name/id of node1
        :param int id2: The name/id of node2
        :param int rating: The level of reliability between A and B
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :param Node|str priority: If there are multiple transactions between the same two nodes,
                                  priority select the action plan (if type(priority) == str)
                                  None select the first edge analysed
        """
        self.__reset_variables()
        self.__nodes[id1].each_add(self.__nodes[id2], rating, timestamp, priority)
        self.__edges.add((id1, id2, rating, timestamp))

    def set_unidirectional_edge(self, id_from, id_to, rating=0, timestamp=0.0, priority=None):
        """
        Add an unidirectional edge from node1 to node2

        :param int id_from: The name/id of node1
        :param int id_to: The name/id of node2
        :param int rating: The level of reliability between A and B
        :param float timestamp: A linux timestamp of a specific rating from A to B
        :param Node|str priority: If there are multiple transactions between the same two nodes,
                                  priority select the action plan (if type(priority) == str)
                                  None select the first edge analysed
        """
        self.__reset_variables()
        self.__nodes[id_from].add(self.__nodes[id_to], rating, timestamp, priority)
        self.__edges.add((id_from, id_to, rating, timestamp))

    # Getters
    def get_edge(self, id_from, id_to):
        """
        Retrieve the edge from the node1 to the node2

        :param int id_from: The name/id of node1
        :param int id_to: The name/id of node2
        :return: Return an immutable dictionary containing the edge or None if nothing as found
        :rtype: MappingProxyType|None
        """
        try:
            edge = self.__nodes[id_from].links[id_to] | {}
            edge["node"] = edge["node"].id
            return MappingProxyType(edge)
        except KeyError:
            return None

    def get_nbr_edges(self):
        """
        :return: The number of edges
        :rtype: int
        """
        return len(self.__edges)

    def get_nbr_nodes(self):
        """
        :return: The number of nodes
        :rtype: int
        """
        return len(self.__nodes)

    ## Graph Generators
    def generate(self, dataframe, is_bidirectional=True, priority=None):
        """
        Generate all the graph (node and edges) from a Pandas Dataframe

        :param pandas.core.frame.DataFrame dataframe: The pandas dataframe containing all the edges and nodes
        :param bool is_bidirectional: if True, generate only bidirectional edges, Else only unidirectional
        :param Node|str priority: If there are multiple transactions between the same two nodes,
                                  priority select the action plan (if type(priority) == str)
                                  None select the first edge analysed
        """
        self.__reset_graph()
        for edge in dataframe.to_numpy():
            buyer, seller, rating, timestamp = int(edge[1]), int(edge[2]), int(edge[3]), float(edge[4])
            self.__nodes.setdefault(buyer, Node(buyer))
            self.__nodes.setdefault(seller, Node(seller))
            if is_bidirectional: self.set_bidirectional_edge(buyer, seller, rating, timestamp, priority)
            else: self.set_unidirectional_edge(buyer, seller, rating, timestamp, priority)


    ## Helpers
    def __sort_edges(self, col: int):
        """
        Sort all the edges based on a specific column
            0. Buyers
            1. Sellers
            2. Rating
            3. Timestamp

        :param int col: Represent the column number to sort the
        :return: A sorted list of edges
        :rtype: list[tuple[4]]
        """
        return sorted(self.__edges, key=lambda x: x[col])


    ## Task 1 Methods
    def get_components(self, compute_local_bridges=False):
        """
        Compute the number of components there are in the current graph
        It saves a list of nodes in each components in 'self.__roots_of_components'
        This Algorithm use BFS and is iterative

        :param bool compute_local_bridges: Check if the method have to compute local bridge as well
        :return: The number of components
        :rtype: int
        """
        if self.__roots_of_components is not None: return len(self.__roots_of_components)
        # Make BFS
        stack = []  # append & pop           # Contains Nodes
        visited = [False for _ in range(len(self.__nodes))]
        self.__roots_of_components = []
        if compute_local_bridges: self.__local_bridges = []

        for node_component in self.__nodes.values():
            if not visited[node_component.id]:
                stack.append(node_component)
                while len(stack) > 0:
                    node = stack.pop()
                    visited[node.id] = True
                    for linked_node in node.links.values():
                        linked_node = linked_node["node"]
                        if not visited[linked_node.id]:
                            if compute_local_bridges and node.strength_bridge(linked_node) == 0.0:
                                self.__local_bridges.append("{} <--> {}".format(node, linked_node))
                            stack.append(linked_node)
                self.__roots_of_components.append(node_component)
        return len(self.__roots_of_components)

    def get_bridges(self):
        """
        Compute the number of bridges there are in the current graph
        It saves a list of edges (str) that are bridges in 'self.__bridges'
        This Algorithm use DFS and is iterative

        :return: The number of bridges
        :rtype: int
        """
        if self.__bridges is not None: return len(self.__bridges)
        def visit_linked_node_dfs():
            """
            Loop into the graph in DFS mode util all the neighbors of the current node are already visited.
            Each time a node is not visited, it is added on top of the stack and it becomes the current node at
            the next loop.

            :pre: The stack is not empty
            :post: The stack is filled with all the nodes looped in a DFS path
            """
            all_deep_visited = False
            while not all_deep_visited:
                node = stack[-1]
                visited[node.id] = True
                first[node.id] = id_time[0]
                best[node.id] = id_time[0] if best[node.id] > id_time[0] else best[node.id]
                id_time[0] += 1

                found = False
                for link in node.links.values():
                    link = link["node"]
                    if visited[link.id]:
                        if link.id != parent_id[node.id]:
                            best[node.id] = min(best[node.id], first[link.id])
                    elif not found:
                        stack.append(link)
                        parent_id[link.id] = node.id
                        found = True
                all_deep_visited = not found

        id_time = [0]
        stack = []
        visited = [False for _ in range(len(self.__nodes))]
        first = [float("Inf") for _ in range(len(self.__nodes))]
        best = [float("Inf") for _ in range(len(self.__nodes))]
        parent_id = [-1 for _ in range(len(self.__nodes))]
        self.__bridges = []

        for main_node in self.__nodes.values():
            if not visited[main_node.id]:
                stack.append(main_node)

                while len(stack) > 0:
                    visit_linked_node_dfs()
                    linked_node = stack.pop()

                    if parent_id[linked_node.id] != -1:
                        parent_node = self.__nodes[parent_id[linked_node.id]]
                        best[parent_node.id] = min(best[parent_node.id], best[linked_node.id])

                        if best[linked_node.id] > first[parent_node.id]:
                            self.__bridges.append("{} <--> {}".format(parent_node, linked_node))

        return len(self.__bridges)

    def get_local_bridges(self):
        """
        Compute the number of local bridges there are in the current graph
        It saves a list of edges (str) that are local bridges in 'self.__local_bridges'
        The method uses the same code used to compute the components

        :return: The number of local bridges
        :rtype: int
        """
        if self.__local_bridges is not None: return len(self.__local_bridges)
        self.get_components(compute_local_bridges=True)
        if self.__local_bridges is None: raise ValueError("Error: Local Bridge")
        return len(self.__local_bridges)


    ## Task 2 Methods
    def get_triadic_closures(self):
        """
        Compute the number of triadic closures there are in the graph from the edge with the median timestamp to the end
        (If you find multiple transactions between the same two nodes, consider only the oldest one according
         to the timestamp).

        :return: The number of triadic closures, a list of triadic closures over time
        :rtype: tuple[int, list[int]]
        """
        sorted_edges = self.__sort_edges(col=3)
        median = len(sorted_edges)//2
        visited_triangle = set()
        overtime_tc, tc = [], 0
        for i_edge in range(median, len(sorted_edges)):
            current_edge = sorted_edges[i_edge]
            node_A, node_B = self.__nodes[current_edge[0]], self.__nodes[current_edge[1]]
            for i_link in node_A.intersection(node_B):
                node_C = self.__nodes[i_link]
                linked_edge_A, linked_edge_B = node_C.links[node_A.id], node_C.links[node_B.id]

                if sorted_edges[median][3] <= linked_edge_A["timestamp"] < current_edge[3]\
                        and sorted_edges[median][3] <= linked_edge_B["timestamp"] < current_edge[3]:
                    if frozenset({node_A.id, node_B.id, node_C.id}) not in visited_triangle:
                        visited_triangle.add(frozenset({node_A.id, node_B.id, node_C.id}))
                        tc += 1

            overtime_tc.append(tc)
        return tc, overtime_tc


    ## Task 3 Methods
    def get_balance_degree(self):
        """
        Compute the balanced degree of the graph from the edge with the median timestamp to the end
        (If there are multiple transactions between the same two nodes, take into account the most recent one,
         that happened before the timestamp you are currently computing).

        :return: The number of triadic closures, a list of triadic closures over time
        :rtype: tuple[float, list[float]]
        """
        def check_triangle_type():
            """
            Check the type of the current triangle A-B-C
            (We consider 0 as a positive value)
            """
            def is_positive(rating): return rating >= 0
            if is_positive(linked_edge_A_B["rating"]):
                if is_positive(linked_edge_A_C["rating"]):
                    if is_positive(linked_edge_B_C["rating"]):
                        triangles_values["balanced"] += 1           # (+ + +)
                    else:
                        triangles_values["unbalanced"] += 1         # (+ + -)
                else:
                    if is_positive(linked_edge_B_C["rating"]):
                        triangles_values["unbalanced"] += 1         # (+ - +)
                    else:
                        triangles_values["balanced"] += 1           # (+ - -)

            else:
                if is_positive(linked_edge_A_C["rating"]):
                    if is_positive(linked_edge_B_C["rating"]):
                        triangles_values["unbalanced"] += 1         # (- + +)
                    else:
                        triangles_values["balanced"] += 1           # (- + -)
                else:
                    if is_positive(linked_edge_B_C["rating"]):
                        triangles_values["balanced"] += 1           # (- - +)
                    else:
                        triangles_values["weak"] += 1               # (- - -)

        def compute_balance_degree():
            """
            Compute the balanced degree of the current graph:
            The equation is the following:

                    (# balanced triangles) + (2/3 * (# weakly balanced triangles))
                ---------------------------------------------------------------------  =  balanced degree
                                   Total number of triangles

            :return: The result of the equation above
            :rtype: float
            """
            sum_triangles = sum(triangles_values.values())
            return (triangles_values["balanced"] + ((2/3) * triangles_values["weak"]))\
                   / (sum_triangles if sum_triangles > 0 else 1)

        sorted_edges = self.__sort_edges(col=3)
        median = len(sorted_edges) // 2
        visited_triangle = set()
        overtime_bd, triangles_values = [], {"balanced": 0, "weak": 0, "unbalanced": 0}

        for i_edge in range(median, len(sorted_edges)):
            current_edge = sorted_edges[i_edge]
            node_A, node_B = self.__nodes[current_edge[0]], self.__nodes[current_edge[1]]
            linked_edge_A_B = node_A.links[node_B.id]
            for i_link in node_A.intersection(node_B):
                node_C = self.__nodes[i_link]
                linked_edge_A_C, linked_edge_B_C = node_A.links[node_C.id], node_B.links[node_C.id]

                if sorted_edges[median][3] <= linked_edge_A_C["timestamp"] < current_edge[3]\
                        and sorted_edges[median][3] <= linked_edge_B_C["timestamp"] < current_edge[3]:
                    if frozenset({node_A.id, node_B.id, node_C.id}) not in visited_triangle:
                        visited_triangle.add(frozenset({node_A.id, node_B.id, node_C.id}))
                        check_triangle_type()

            overtime_bd.append(compute_balance_degree())
        return compute_balance_degree(), overtime_bd
