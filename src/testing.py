import networkx as nx
from graph import Graph
import csv_extraction

def nx_easy_example1() -> nx.classes.graph.Graph:
    """
    An easy example to tests the first graph methods with NetworkX

    :return: An instance of the NetworkX Graph class
    :rtype: nx.classes.graph.Graph
    """
    graph_instance = nx.Graph()
    for i in range(8):
        graph_instance.add_node(i)

    graph_instance.add_edge(0, 1)
    graph_instance.add_edge(0, 2)
    # graph_instance.add_edge(0, 3)
    # graph_instance.add_edge(1, 2)
    graph_instance.add_edge(1, 3)
    graph_instance.add_edge(2, 3)

    graph_instance.add_edge(1, 4)

    graph_instance.add_edge(4, 5)
    graph_instance.add_edge(4, 6)
    graph_instance.add_edge(4, 7)
    graph_instance.add_edge(5, 6)
    graph_instance.add_edge(5, 7)
    graph_instance.add_edge(6, 7)
    return graph_instance

def nx_complete_dataset_graph() -> nx.classes.graph.Graph:
    """
    The complete dataset provided during the project implemented with NetworkX

    :return: An instance of the NetworkX Graph class
    :rtype: nx.classes.graph.Graph
    """
    graph_instance = nx.Graph()
    dataset = csv_extraction.DatasetTreatment()
    dataset.open()
    for edge in dataset.get().to_numpy():
        id1, id2 = int(edge[1]), int(edge[2])
        if id1 not in graph_instance.nodes:
            graph_instance.add_node(id1)
        if id2 not in graph_instance.nodes:
            graph_instance.add_node(id2)
        graph_instance.add_edge(id1, id2)
    dataset.close()
    return graph_instance

def easy_example1() -> Graph:
    """
    An easy example to tests all the graph methods

    :return: An instance of the Graph class
    :rtype: Graph
    """
    graph_instance = Graph()
    for i in range(8):
        graph_instance.set_node(i)

    graph_instance.set_bidirectional_edge(0, 1, rating=-5, timestamp=0.0)
    graph_instance.set_bidirectional_edge(0, 2, rating=-8, timestamp=10.0)
    graph_instance.set_bidirectional_edge(0, 3, rating=10, timestamp=2.0)
    graph_instance.set_bidirectional_edge(1, 2, rating=4, timestamp=30.0)
    graph_instance.set_bidirectional_edge(1, 3, rating=1, timestamp=4.0)
    graph_instance.set_bidirectional_edge(2, 3, rating=8, timestamp=50.0)

    graph_instance.set_bidirectional_edge(1, 4, rating=7, timestamp=6.0)
    # graph_instance.set_bidirectional_edge(1, 5, rating=-8, timestamp=7.0)

    # graph_instance.set_bidirectional_edge(3, 6, rating=-1, timestamp=8.0)

    graph_instance.set_bidirectional_edge(4, 5, rating=2, timestamp=1.0)
    graph_instance.set_bidirectional_edge(4, 6, rating=-3, timestamp=3.0)
    graph_instance.set_bidirectional_edge(4, 7, rating=4, timestamp=5.0)
    graph_instance.set_bidirectional_edge(5, 6, rating=-5, timestamp=12.0)
    graph_instance.set_bidirectional_edge(5, 7, rating=6, timestamp=22.0)
    graph_instance.set_bidirectional_edge(6, 7, rating=-7, timestamp=14.0)

    # graph_instance.set_bidirectional_edge(0, 5, rating=8, timestamp=15.0)
    return graph_instance

def complete_dataset_graph() -> Graph:
    """
    The complete dataset provided during the project

    :return: An instance of the Graph class
    :rtype: Graph
    """
    graph_instance = Graph()
    dataset = csv_extraction.DatasetTreatment()
    dataset.open()
    graph_instance.generate(dataset.get())
    dataset.close()
    return graph_instance

if __name__ == "__main__":
    nx_graph = nx_easy_example1()
    # nx_graph = nx_complete_dataset_graph()

    # print(nx.number_connected_components(nx_graph))      # Components    => 4
    # print(len(list(nx.bridges(nx_graph))))               # Bridges       => 2290
    # print(len(list(nx.local_bridges(nx_graph))))         # Local Bridges => 6405

    # graph = easy_example1()
    graph = complete_dataset_graph()

    print("#Components \t\t=>\t", graph.get_components(compute_local_bridges=True))
    print("#Bridges \t\t\t=>\t", graph.get_bridges())
    print("#Local Bridges \t\t=>\t", graph.get_local_bridges())
    print()
    print("#Triadic Closures \t=>\t", graph.get_triadic_closures()[0])
    print()
    print("#Balanced Degree \t=>\t", graph.get_balance_degree()[0])
