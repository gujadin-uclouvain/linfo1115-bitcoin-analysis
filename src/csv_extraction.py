import pandas as pd

class DatasetTreatment:
    def __init__(self):
        self.__filename = "./dataset.csv"
        self.__file = None
        self.__dataset = None

    def open(self):
        self.__file = open(self.__filename, "r")
        self.__dataset = pd.read_csv(self.__file)

    def close(self):
        self.__file.close()
        self.__file = None
        self.__dataset = None

    def get(self):
        return self.__dataset
